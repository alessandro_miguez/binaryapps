package edu.afmiguez.tk.numeric_representation.models.mips;

import org.junit.Test;

import static org.junit.Assert.*;

public class JInstructionTest {
    @Test
    public void name() {
        JInstruction jInstruction= (JInstruction) InstructionAbstract.getInstruction("j 0x040000c");
        assertNotNull(jInstruction);
        assertEquals("j 0x0040000C",jInstruction.getInstruction());
    }
}