package edu.afmiguez.tk.numeric_representation.models;

import org.junit.Test;

import static org.junit.Assert.*;

public class BinaryTest {

    @Test
    public void convertToDecimal() throws Exception{
        Binary binary=new Binary();
        binary.setValue("1000");
        assertEquals("8",binary.convertToDecimal(false));

        binary.setValue("1001");
        assertEquals("9",binary.convertToDecimal(false));
        assertEquals("-7",binary.convertToDecimal(true));


        binary.setValue("1111");
        assertEquals("15",binary.convertToDecimal(false));
        assertEquals("-1",binary.convertToDecimal(true));

        binary.setValue("0111");
        assertEquals("7",binary.convertToDecimal(false));
        assertEquals("7",binary.convertToDecimal(true));

    }

    @Test
    public void testOperations(){
        String str="0000";
        Binary bin=new Binary(str);
        for(int i=0;i<16;i++){
            assertEquals(i,Integer.parseInt(bin.convertToDecimal(false)));
            bin.sumOne();
        }
        try {
            bin.setValue("1111");
        } catch (Exception e) {
            e.printStackTrace();
        }
        for(int i=15;i>0;i--){
            assertEquals(i,Integer.parseInt(bin.convertToDecimal(false)));
            bin.minusOne();
        }
        try {
            bin.setValue("00010");
        } catch (Exception e) {
            e.printStackTrace();
        }
        bin.add(new Binary("110"),false);
        assertEquals("01000",bin.getValue());
        bin.subtract(new Binary("11"),false);
        assertEquals("00101",bin.getValue());

    }
}