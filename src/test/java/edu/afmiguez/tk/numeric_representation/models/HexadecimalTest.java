package edu.afmiguez.tk.numeric_representation.models;

import org.junit.Test;

import static org.junit.Assert.*;

public class HexadecimalTest {

    @Test
    public void convertToDecimal() throws Exception{
        Hexadecimal hex=new Hexadecimal();
        hex.setValue("FFFF");
        assertEquals("1111111111111111",hex.convertToBinary());
        assertEquals(""+(int)(Math.pow(2,16)-1),hex.convertToDecimal(false));
        assertEquals(""+(int)(Math.pow(2,15)-1-Math.pow(2,15)),hex.convertToDecimal(true));

        hex.setValue("F000");
        assertEquals("1111000000000000",hex.convertToBinary());
        assertEquals(""+(int)(15*Math.pow(16,3)),hex.convertToDecimal(false));
        assertEquals(""+(int)((Math.pow(2,14)+Math.pow(2,13)+Math.pow(2,12))-Math.pow(2,15)),hex.convertToDecimal(true));

        hex.setValue("A8000604");
        assertEquals("10101000000000000000011000000100",hex.convertToBinary());
        assertEquals("2818573828",hex.convertToDecimal(false));
        assertEquals("-1476393468",hex.convertToDecimal(true));

        hex.setValue("7000AB0F");
        assertEquals("01110000000000001010101100001111",hex.convertToBinary());
        assertEquals("1879091983",hex.convertToDecimal(false));
        assertEquals("1879091983",hex.convertToDecimal(true));

        hex.setValue("8000AB0F");
        assertEquals("10000000000000001010101100001111",hex.convertToBinary());
        assertEquals("2147527439",hex.convertToDecimal(false));
        assertEquals("-2147439857",hex.convertToDecimal(true));
    }

    @Test(expected = Exception.class)
    public void testException() throws Exception {
        Hexadecimal hex=new Hexadecimal();
        hex.setValue("G");
        hex.convertToDecimal(false);
    }
}