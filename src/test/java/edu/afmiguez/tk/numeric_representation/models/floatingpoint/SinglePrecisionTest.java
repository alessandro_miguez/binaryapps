package edu.afmiguez.tk.numeric_representation.models.floatingpoint;

import edu.afmiguez.tk.numeric_representation.models.Binary;
import edu.afmiguez.tk.numeric_representation.models.Hexadecimal;
import edu.afmiguez.tk.numeric_representation.models.mips.InstructionAbstract;
import org.junit.Test;

import static org.junit.Assert.*;

public class SinglePrecisionTest {

    @Test
    public void convertToDecimal() throws Exception {
        SinglePrecision singlePrecision=new SinglePrecision("0xad100002");
        assertEquals("-8.18545405067983E-12",singlePrecision.convertToDecimal(false));
        singlePrecision.setValue(new Hexadecimal("0x41a40000"));
        assertEquals("20.5",singlePrecision.convertToDecimal(false));
        singlePrecision.setValue(new Hexadecimal("0xbf547ae1"));
        assertEquals(-0.83,Float.valueOf(singlePrecision.convertToDecimal(false)),0.0000001);
    }

    @Test
    public void convertFloatToHexa() {
        SinglePrecision singlePrecision=new SinglePrecision("0xad100002");
       assertEquals("41A40000",singlePrecision.convertFloatToHexa(20.5f).getValue());
        assertEquals("BF547AE1",singlePrecision.convertFloatToHexa(-0.83f).getValue());
        assertEquals("BF555555",singlePrecision.convertFloatToHexa(-5/6f).getValue());

        System.out.println(new Binary("10101101000100000000000000000010").convertToDecimal(true));
        System.out.println(new Binary("10101101000100000000000000000010").convertToDecimal(false));
        try {
            System.out.println(new SinglePrecision("10101101000100000000000000000010").convertToDecimal(true));
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(InstructionAbstract.convertHexadecimalToInstruction(new Hexadecimal("10101101000100000000000000000010")).getInstruction());
//        System.out.println(new Hexadecimal("10101101000100000000000000000010").getValue());
    }
}