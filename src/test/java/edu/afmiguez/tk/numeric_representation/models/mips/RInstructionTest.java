package edu.afmiguez.tk.numeric_representation.models.mips;

import org.junit.Test;

import static org.junit.Assert.*;

public class RInstructionTest {

    @Test
    public void name() {
        InstructionAbstract rInstruction=InstructionAbstract.getInstruction("add $t0,$t0,$t1");
        assert rInstruction != null;
        assertEquals("01094020",rInstruction.getHexadecimal().getValue());

        rInstruction=InstructionAbstract.getInstruction("add $s0,$0,$0");
        assert rInstruction != null;
        assertEquals("00008020",rInstruction.getHexadecimal().getValue());

        rInstruction=InstructionAbstract.getInstruction("add $s0,$s0,$t0");
        assert rInstruction != null;
        assertEquals("02088020",rInstruction.getHexadecimal().getValue());
    }
}