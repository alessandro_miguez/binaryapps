package edu.afmiguez.tk.numeric_representation.models.arq;

import org.junit.Test;

import static org.junit.Assert.*;

public class StopAndWaitTest {

    @Test
    public void test(){
        StopAndWait stopAndWait=new StopAndWait(null,null,null,4000f,null,0.02f,null,0.5f);
        assertEquals(160,stopAndWait.getFrameLenght().intValue());
    }
}