package edu.afmiguez.tk.numeric_representation.models;

import org.junit.Test;

import static org.junit.Assert.*;

public class HammingCodeTest {

    @Test
    public void getCodedBinary() {
        String code="10101111";
        HammingCode hammingCode=new HammingCode(new Binary(code));
        assertEquals("101001001111",hammingCode.getCodedBinary().getValue());
        assertEquals(HammingCode.correctCode(hammingCode.getCodedBinary().getValue()),hammingCode.getCodedBinary().getValue());
        assertEquals(code,HammingCode.getMessage(HammingCode.correctCode(hammingCode.getCodedBinary().getValue())));

        assertEquals(HammingCode.correctCode("111001001111"),hammingCode.getCodedBinary().getValue());
        assertEquals(code,HammingCode.getMessage(HammingCode.correctCode(hammingCode.getCodedBinary().getValue())));

        assertEquals(HammingCode.correctCode("101101001111"),hammingCode.getCodedBinary().getValue());
        assertEquals(code,HammingCode.getMessage(HammingCode.correctCode(hammingCode.getCodedBinary().getValue())));

        assertEquals(HammingCode.correctCode("101001001011"),hammingCode.getCodedBinary().getValue());
        assertEquals(code,HammingCode.getMessage(HammingCode.correctCode(hammingCode.getCodedBinary().getValue())));

        code="1011001";
        hammingCode=new HammingCode(new Binary(code));
        assertEquals("10100111001",hammingCode.getCodedBinary().getValue());
        assertEquals(HammingCode.correctCode(hammingCode.getCodedBinary().getValue()),hammingCode.getCodedBinary().getValue());
        assertEquals(code,HammingCode.getMessage(HammingCode.correctCode(hammingCode.getCodedBinary().getValue())));

        code="1100";
        hammingCode=new HammingCode(new Binary(code));
        assertEquals("0111100",hammingCode.getCodedBinary().getValue());
        assertEquals(HammingCode.correctCode(hammingCode.getCodedBinary().getValue()),hammingCode.getCodedBinary().getValue());
        assertEquals(code,HammingCode.getMessage(HammingCode.correctCode(hammingCode.getCodedBinary().getValue())));

        code="11001111";
        hammingCode=new HammingCode(new Hexadecimal(code));
        assertEquals("68F",new Hexadecimal(hammingCode.getCodedBinary().getValue()).getValue());

        code="10101111";
        hammingCode=new HammingCode(new Hexadecimal(code));
        assertEquals("A4F",new Hexadecimal(hammingCode.getCodedBinary().getValue()).getValue());

        code="111001001111";
        assertEquals("A4F",new Hexadecimal(HammingCode.correctCode(code)).getValue());
    }
}