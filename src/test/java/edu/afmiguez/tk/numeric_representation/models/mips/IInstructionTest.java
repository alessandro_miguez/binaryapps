package edu.afmiguez.tk.numeric_representation.models.mips;

import org.junit.Test;

import static org.junit.Assert.*;


public class IInstructionTest {
    @Test
    public void name() {
        IInstruction iInstruction= (IInstruction) InstructionAbstract.getInstruction("addi $t1,$0,1000");
        assert iInstruction != null;
        assertEquals("200903E8",iInstruction.getHexadecimal().getValue());
    }
}