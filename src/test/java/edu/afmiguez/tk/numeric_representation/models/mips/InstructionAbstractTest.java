package edu.afmiguez.tk.numeric_representation.models.mips;

import edu.afmiguez.tk.numeric_representation.models.Hexadecimal;
import org.junit.Test;

import static org.junit.Assert.*;


public class InstructionAbstractTest {
    @Test
    public void name() {
        String [] registers={"$0","$at","$v0","$v1","$a0","$a1","$a2","$a3","$t0","$t1","$t2","$t3","$t4","$t5","$t6","$t7","$s0","$s1","$s2","$s3","$s4","$s5","$s6","$s7","$t8","$t9","$k0","$k1","$gp","$sp","$fp","$ra"};
        for(int i=0;i<registers.length;i++){
            String register=registers[i];
            assertEquals(i,Integer.parseInt(InstructionAbstract.getRegisterBinary(register).convertToDecimal(false)));
        }
    }

    @Test
    public void instructions() {

        for(String detailsKey:InstructionAbstract.getInstructionDetailsMap().keySet()){
            InstructionDetails details=InstructionAbstract.getInstructionDetailsMap().get(detailsKey);
            if(details.getType().equals("R")){
                assertNotNull(details.getFunction());
            }else{
                assertNull(details.getFunction());
            }
        }
    }

    @Test
    public void testConvertHexadecimalToInstruction() {
        assertEquals("add $s0,$s0,$t0",InstructionAbstract.convertHexadecimalToInstruction(new Hexadecimal("0x"+"02088020")).getInstruction());
        assertEquals("add $s0,$0,$0",InstructionAbstract.convertHexadecimalToInstruction(new Hexadecimal("0x"+"00008020")).getInstruction());
        assertEquals("addi $t0,$0,1",InstructionAbstract.convertHexadecimalToInstruction(new Hexadecimal("0x"+"20080001")).getInstruction());
        assertEquals("addi $t1,$0,1000",InstructionAbstract.convertHexadecimalToInstruction(new Hexadecimal("0x"+"200903e8")).getInstruction());
    }
}