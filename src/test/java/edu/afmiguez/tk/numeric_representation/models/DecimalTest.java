package edu.afmiguez.tk.numeric_representation.models;

import org.junit.Test;

import static org.junit.Assert.*;

public class DecimalTest {

    @Test
    public void test(){
        Decimal dec=new Decimal();
        dec.setValue("10");
        assertEquals("1010",dec.convertToBinary(false,4).getValue());

        assertTrue(dec.isConvertionValid(4,false));
        assertFalse(dec.isConvertionValid(3,false));

        assertFalse(dec.isConvertionValid(4,true));

        dec.setValue("2147527439");
        assertEquals("10000000000000001010101100001111",dec.convertToBinary(false,32).getValue());
    }
}