package edu.afmiguez.tk.numeric_representation.controllers;

import edu.afmiguez.tk.numeric_representation.models.mips.InstructionAbstract;
import edu.afmiguez.tk.numeric_representation.services.ACPTService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

//@RunWith(MockitoJUnitRunner.class)
public class ACPTControllerTest {

    //private MockMvc mockMvc;

    @Mock
    private ACPTService acptService;

    @InjectMocks
    private ACPTController acptController;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        //mockMvc =
        MockMvcBuilders
                .standaloneSetup(acptController)
                .build();
    }

    @Test
    public void test(){
        when(acptService.convertInstruction("add $t0,$t0,$t1")).thenReturn(InstructionAbstract.getInstruction("add $t0,$t0,$t1"));
        assertEquals("01094020",acptService.convertInstruction("add $t0,$t0,$t1").getHexadecimal().getValue());
        verify(acptService).convertInstruction("add $t0,$t0,$t1");
    }

}