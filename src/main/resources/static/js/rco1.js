$(function () {

    var distanceSel = $("#distance");
    var velSel = $("#velocity");
    var flSel = $("#framelenght");
    var bitSel = $("#bitrate");
    var tpSel = $("#tp");
    var tfSel = $("#tf");
    var aSel = $("#a");
    var uSel = $("#utilization");
    var wSel = $("#window_number");
    var frameBitsSel = $("#frameBits");

    var divResultsSel = $("#results");

    /*bit_sel.val("4000");
    tp_sel.val("0.02");
    u_sel.val("0.5");
    */

    /*
    fl_sel.val(512);
    bit_sel.val(1544000);
    distance_sel.val(3000000);
    vel_sel.val(16700000000);
    */

    /*
    fl_sel.val(1000);
    bit_sel.val(1000000);
    tp_sel.val(0.270);
*/

    // bitSel.val(1000000);
    // distanceSel.val(100);
    // flSel.val(12000);
    // velSel.val(200000000);


    $("#calculate").click(function (event) {
        event.preventDefault();

        $(divResultsSel).children().remove();

        var distance = distanceSel.val() === "" ? null : distanceSel.val();
        var velocity = velSel.val() === "" ? null : velSel.val();
        var framelenght = flSel.val() === "" ? null : flSel.val();
        var bitrate = bitSel.val() === "" ? null : bitSel.val();
        var fp = tpSel.val() === "" ? null : tpSel.val();
        var ft = tfSel.val() === "" ? null : tfSel.val();
        var a = aSel.val() === "" ? null : aSel.val();
        var utilization = uSel.val() === "" ? null : uSel.val();
        var w = wSel.val() === "" ? 1 : wSel.val();
        var frame_bits = frameBitsSel.val() === "" ? 1 : frameBitsSel.val();

        var request = {
            distance,
            velocity,
            framelenght,
            bitrate,
            fp,
            ft,
            a,
            u: utilization,
            windows: w,
            bits: frame_bits
        };


        $.ajax({
            url: location.origin + "/arq",
            type: "put",
            data: JSON.stringify(request),
            dataType: "json",
            contentType: "application/json"

        }).done(function (data) {
            for (var i = 0; i < data.length; i++) {
                var divResult = $("<div>", {
                    class: "form-row"
                });

                var divDistance = $("<div>", {
                    class: "col"
                });

                /* $("<input>", {
                     type: "text',
                     val: $('#div1').text()
                 })*/
                $(divDistance).append($("<input/>", {
                        id: "distance" + i,
                        value: data[i].distance,
                        class: "form-control",
                    placeholder:"Distance"
                    })
                );

                var divVelocity = $("<div>", {
                    class: "col"
                });

                $(divVelocity).append($("<input/>", {
                        id: "velocity" + i,
                        value: data[i].velocity,
                        class: "form-control",
                    placeholder:"Velocity"
                    })
                );

                var divFrameLenght = $("<div>", {
                    class: "col"
                });
                $(divFrameLenght).append($("<input/>", {
                        id: "frameLenght" + i,
                        value: data[i].frameLenght,
                        class: "form-control",
                    placeholder:"Frame Lenght"
                    })
                );

                var divBitrate = $("<div>", {
                    class: "col"
                });
                $(divBitrate).append("<input/>", {
                    id: "bitrate" + i,
                    value: data[i].bitrate,
                    class: "form-control",
                    placeholder:"Bit Rate"
                });

                var divTp = $("<div>", {
                    class: "col"
                });
                $(divTp).append($("<input/>", {
                        id: "tp" + i,
                        value: data[i].tp,
                        class: "form-control",
                    placeholder:"Propagation time"
                    })
                );

                var divTf = $("<div>", {
                    class: "col"
                });
                $(divTf).append($("<input/>", {
                        id: "tf" + i,
                        value: data[i].tf,
                        class: "form-control",
                    placeholder:"Frame time"
                    })
                );

                var divA = $("<div>", {
                    class: "col"
                });
                $(divA).append($("<input/>", {
                        id: "a" + i,
                        value: data[i].a,
                        class: "form-control",
                    placeholder:"A"
                    })
                );

                var divU = $("<div>", {
                    class: "col"
                });
                $(divU).append($("<input/>", {
                        id: "u" + i,
                        value: data[i].u,
                        class: "form-control",
                    placeholder:"Utilization"
                    })
                );

                var divWindows = $("<div>", {
                    class: "col"
                });
                $(divWindows).append($("<input/>", {
                        id: "windows" + i,
                        value: data[i].windows,
                        class: "form-control",
                    placeholder:"Windows"
                    })
                );

                var divBits = $("<div>", {
                    class: "col"
                });
                $(divBits).append($("<input/>", {
                        id: "bits" + i,
                        value: data[i].bits,
                        class: "form-control",
                    placeholder:"Sequence bits"
                    })
                );

                divResult.append($(divDistance));
                divResult.append($(divVelocity));
                divResult.append($(divFrameLenght));
                divResult.append($(divA));
                divResult.append($(divTp));
                divResult.append($(divTf));
                divResult.append($(divA));
                divResult.append($(divU));
                divResult.append($(divWindows));
                divResult.append($(divBits));

                divResultsSel.append("<h5>Result "+data[i].name+"</h5>");
                divResultsSel.append(divResult);

                /*
                var div = $("<div>", {
                    id: "div' + algorithms[i],
                });
                $(div).append("<h1/>").html(algorithms[i]);
                 */

                /*
                                console.log(data[i]);
                                distance_sel.val(data[i].distance);
                                vel_sel.val(data[i].velocity);
                                fl_sel.val(data[i].frameLenght);
                                bit_sel.val(data[i].bitrate);
                                tp_sel.val(data[i].tp);
                                tf_sel.val(data[i].tf);
                                a_sel.val(data[i].a);
                                u_sel.val(data[i].u);

                                w_sel.val(data[i].windows);
                                frame_bits_sel.val(data[i].bits);
                                */
            }
        }).fail(function (data) {
            // alert("Error");
        });

    });
});