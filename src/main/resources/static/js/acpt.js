$(function(){
    var hexadecimalSel = $("#hexadecimal");
    var instructionSel = $("#instruction");

    var hexadecimalResultSel=$("#hexadecimalResult");
    var instructionResultSel=$("#instructionResult");

    function appendDiv(data,div){
        for(var key in data){
            if (data.hasOwnProperty(key)) {
                var divResult;
                if(key==="instruction"){
                     divResult= $("<div>", {
                        class: "col",
                        text:key+": "+data[key]
                    });
                }else{
                    divResult = $("<div>", {
                        class: "col",
                        text:key+": "+data[key].value
                    });
                }
                div.append(divResult);
            }
        }
    }


    $("#convertToInstruction").click(function (event) {
        $(hexadecimalResultSel).children().remove();
        event.preventDefault();
        $.ajax({
            url:location.origin+"/acpt/hexadecimal",
            type:"put",
            contentType: "text/plain",
            data:hexadecimalSel.val()
        }).done(function(data){
            appendDiv(data,hexadecimalResultSel);
        });
    });

    $("#convertToHexadecimal").click(function (event) {
        $(instructionResultSel).children().remove();
        event.preventDefault();
        $.ajax({
            url:location.origin+"/acpt/instruction",
            type:"put",
            contentType: "text/plain",
            // dataType:"text/plain",
            data:instructionSel.val()
        }).done(function(data){
            appendDiv(data,instructionResultSel);
        });
    });
});