package edu.afmiguez.tk.numeric_representation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NumericRepresentationApplication {

    public static void main(String[] args) {
        SpringApplication.run(NumericRepresentationApplication.class, args);
    }
}
