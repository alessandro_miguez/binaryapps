package edu.afmiguez.tk.numeric_representation.models.mips;

import edu.afmiguez.tk.numeric_representation.models.Binary;
import edu.afmiguez.tk.numeric_representation.models.NumericRepresentationI;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class IInstruction extends InstructionAbstract{
    private Binary optcode;
    private Binary rs;
    private Binary rt;
    private Binary immediate;

    IInstruction(NumericRepresentationI numericRepresentationI) {
        super(numericRepresentationI);
    }

    IInstruction(String instruction) {
        this();
        parseBinary(instruction);
    }

    private IInstruction(){}

    @Override
    public void parseBinary(String binary) {
        this.optcode=new Binary(binary.substring(0,6));
        this.rs=new Binary(binary.substring(6,11));
        this.rt=new Binary(binary.substring(11,16));
        this.immediate=new Binary(binary.substring(16));
    }

    @Override
    public Binary getBinary() {
        return new Binary(optcode.getValue()+rs.getValue()+rt.getValue()+immediate.getValue());
    }

    @Override
    public String getInstruction() {
        InstructionDetails instructionDetails=InstructionAbstract.optFunctInstructionDetailsMap.get(this.getOptcode().getValue());
        if(isShift(instructionDetails.getName())){
            return instructionDetails.getName()+" "+reverseRegisters.get(getRt().getValue())+","+reverseRegisters.get(getRs().getValue())+","+immediate.convertToDecimal(true);
        }
        return null;
    }

}
