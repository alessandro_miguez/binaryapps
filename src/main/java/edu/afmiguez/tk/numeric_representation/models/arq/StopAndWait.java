package edu.afmiguez.tk.numeric_representation.models.arq;

import lombok.Data;

@Data
public class StopAndWait implements ARQAlgorithms{

    protected Float distance;
    protected Float velocity;

    protected Float frameLenght;
    protected Float bitrate;

    protected Float tf;
    protected Float tp;

    protected Float a;
    protected Float u;

    public StopAndWait() {
    }

    public StopAndWait(StopAndWait stopAndWait) {
        this(stopAndWait.distance,stopAndWait.velocity,stopAndWait.frameLenght,stopAndWait.bitrate,stopAndWait.tf,stopAndWait.tp,stopAndWait.a,stopAndWait.u);
    }

    public StopAndWait(Float distance, Float velocity, Float frameLenght, Float bitrate, Float tf, Float tp, Float a, Float u) {
        this.distance = distance;
        this.velocity = velocity;
        this.frameLenght = frameLenght;
        this.bitrate = bitrate;
        this.tf = tf;
        this.tp = tp;
        this.a = a;
        this.u = u;
        for(int i=0;i<3;i++){
            calculate();
        }
    }

    private void calculate() {
        if(this.frameLenght!=null && this.bitrate!=null){
            this.tf=tf==null?frameLenght/bitrate:tf;
        }else{
            if(this.frameLenght==null && this.bitrate!=null && this.tf!=null){
                this.frameLenght=this.tf*this.bitrate;
            }
            if(this.bitrate==null && this.frameLenght!=null && this.tf!=null){
                this.bitrate=this.frameLenght/this.tf;
            }
        }
        if(this.distance!=null && this.velocity!=null){
            this.tp=tp==null?distance/velocity:tp;
        }else{
            if(this.distance==null && this.velocity!=null && this.tp!=null){
                this.distance=this.tp*this.velocity;
            }
            if(this.velocity==null && this.distance!=null && this.tp!=null){
                this.velocity=this.distance/this.tp;
            }
        }
        if(this.tp!=null && this.tf!=null){
            this.a=a==null?this.tp/this.tf:a;
        }else{
            if(this.a!=null && this.tp!=null){
                this.tf=this.tp/this.a;
            }
            if(this.a!=null && this.tf!=null){
                this.tp=this.a*this.tf;
            }
        }
        if(this.a!=null){
            this.u=u==null?1/(1+2*this.a):u;
        }else{
            if(this.u!=null){
                this.a=(float)(1/(2*this.u)-0.5);
            }
        }
    }

    @Override
    public float getUtilization() {
        return this.getU()<1?this.getU():1;
    }

    @Override
    public float getFrameTime() {
        return this.getTf();
    }

    @Override
    public float getPropagationTime() {
        return this.getTp();
    }

    @Override
    public String getName() {
        return "Stop And Wait";
    }

}
