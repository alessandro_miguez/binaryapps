package edu.afmiguez.tk.numeric_representation.models.mips;

import edu.afmiguez.tk.numeric_representation.models.Binary;
import edu.afmiguez.tk.numeric_representation.models.Hexadecimal;

public interface InstructionI {
    Binary getBinary();
    Hexadecimal getHexadecimal();
    String getInstruction();
    void parseBinary(String binary);
}
