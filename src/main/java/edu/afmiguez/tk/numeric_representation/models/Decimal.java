package edu.afmiguez.tk.numeric_representation.models;

import lombok.Data;

@Data
public class Decimal {

    private String value;

    public Decimal(String value) {
        this.value = value;
    }

    Decimal(){

    }

    public Binary convertToBinary(boolean is2Complement,int nbits)  {
        Binary bin=new Binary();
        long value=Long.parseLong(this.value);
        boolean isNegative=value<0;
        value=Math.abs(value);
        StringBuilder sb=new StringBuilder();
        while(value>0){
            sb.append(value%2);
            value=value/2;
        }
        if(sb.length()<nbits){
            int padding=nbits-sb.length();
            for(int i=0;i<padding;i++){
                sb.append('0');
            }
        }
//        if(is2Complement){
//            if(isNegative){
//                sb.append("1");
//            }else{
//                sb.append("0");
//            }
//        }
        try {
            bin.setValue(""+sb.reverse());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        if(isNegative){
            bin.not();
            bin.sumOne();
        }
        return bin;
    }

    public int nBits(boolean is2Complement) {
        if(is2Complement){
            return (int)(Math.log(Integer.parseInt(this.value))/Math.log(2));
        }
        return (int)(Math.log(Integer.parseInt(this.value))/Math.log(2))+1;
    }

    protected boolean isConvertionValid(int nbits, boolean is2Complement){
        int bitsNeeded=this.nBits(is2Complement);
        return is2Complement?
                nbits>bitsNeeded+1:
                nbits>=bitsNeeded;
    }
}
