package edu.afmiguez.tk.numeric_representation.models.arq;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SlidingWindow extends StopAndWait {

    private Integer windows;
    private Integer bits;

    public SlidingWindow(SlidingWindow stopAndWait) {
        super(stopAndWait);
        this.windows=stopAndWait.windows;
        this.bits=stopAndWait.bits;
        if(this.windows==1 && this.bits!=2){
            this.windows=((int)Math.pow(2,this.bits));
        }
        if(this.windows!=1 && this.bits==2){
            this.bits=(int)(Math.log10(this.windows)/Math.log10(2));
        }
        this.setU(this.getU());
    }

    private SlidingWindow(Float distance, Float velocity, Float frameLenght, Float bitrate, Float tf, Float tp, Float a, Float u, Integer windows, Integer bits) {
        super(distance, velocity, frameLenght, bitrate, tf, tp, a, u);
        this.windows = windows;
        this.bits = bits;

    }

    @Override
    public Float getU() {
        return this.windows < (1+2*this.getA()) ?
                this.windows*super.getU()>1?1:this.windows*super.getU()
                :1;
    }

    public static void main(String[] args) {
        /*
        R=10^6
        d=100m
        L=1500*8
        v=2*10^8
         */
        StopAndWait stopAndWait=new StopAndWait(100f,200000000f,1500*8f,1000000f,null,null,null,null);
        System.out.println(stopAndWait.getTf());
        System.out.println(stopAndWait.getTp());
        System.out.println(stopAndWait.getA());
        System.out.println(stopAndWait.getU());

        System.out.println();

        SlidingWindow slidingWindow=new SlidingWindow(100f,200000000f,1500*8f,1000000f,null,null,null,null,16,null);
        System.out.println(slidingWindow.getTf());
        System.out.println(slidingWindow.getTp());
        System.out.println(slidingWindow.getA());
        System.out.println(slidingWindow.getU());

        /*
        L=1000
        R=10^6
        Tp=270*10^-3
         */
        System.out.println();

        stopAndWait=new StopAndWait(null,null,1000f,1000000f,null,0.270f,null,null);
        System.out.println(stopAndWait.getTf());
        System.out.println(stopAndWait.getTp());
        System.out.println(stopAndWait.getA());
        System.out.println(stopAndWait.getU());

        System.out.println();

        slidingWindow=new SlidingWindow(null,null,1000f,1000000f,null,0.270f,null,null,8,null);
        System.out.println(slidingWindow.getTf());
        System.out.println(slidingWindow.getTp());
        System.out.println(slidingWindow.getA());
        System.out.println(slidingWindow.getU());
    }

    @Override
    public String getName() {
        return "Sliding Window";
    }
}
