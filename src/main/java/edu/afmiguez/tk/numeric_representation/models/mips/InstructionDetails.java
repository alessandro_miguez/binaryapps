package edu.afmiguez.tk.numeric_representation.models.mips;

import lombok.Data;

@Data
public class InstructionDetails{
    private String name;
    private String type;
    private String optcode;
    private String function;

    protected InstructionDetails(String[] details) {
        if(details.length<3 || details.length>4){
            try {
                throw new IllegalArgumentException("invalid number of details");
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        this.name=details[0];
        this.type=details[1];
        this.optcode=details[2];
        if(details.length==4){
            this.function=details[3];
        }
    }

    public InstructionDetails(String name, String type, String optcode, String function) {
        this.name = name;
        this.type = type;
        this.optcode = optcode;
        this.function = function;
    }
}