package edu.afmiguez.tk.numeric_representation.models;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Data
public class HammingCode {

    protected static Logger logger = LoggerFactory.getLogger(HammingCode.class);

    private NumericRepresentationI binaryToCode;

    HammingCode(NumericRepresentationI binaryToCode) {
        this.binaryToCode = binaryToCode;
    }

    protected Binary getCodedBinary() {
        int nParityBits = (int) Math.ceil(Math.log(this.binaryToCode.convertToBinary(true).length()) / Math.log(2)) + 1;
        Binary binary = new Binary(this.binaryToCode.convertToBinary(true));
        for (int i = 0; i < nParityBits; i++) {
            binary.stuffBit(" ", ((int) Math.pow(2, i)) - 1);
        }
        for (int i = 0; i < nParityBits; i++) {
            String value = binary.getValue();
            int offset = (int) Math.pow(2, i);
            String message = getBitsToCheckParity(value, offset);
            String bitToStuff;
            if (isPair(message)) {
                bitToStuff = "0";
            } else {
                bitToStuff = "1";
            }
            binary.changeBit(bitToStuff, offset - 1);
        }
        return binary;
    }

    private static boolean isPair(String message) {
        int count = 0;
        for (char c : message.toCharArray()) {
            if (c == '1') {
                count++;
            }
        }
        return count % 2 == 0;
    }

    protected static String getMessage(String code) {
        String correctCode = correctCode(code);
        int nParityBits = (int) Math.floor(Math.log(correctCode.length()) / Math.log(2)) + 1;
        for (int i = 0; i < nParityBits; i++) {
            int parityBitPosition = (int) Math.pow(2, i) - 1;
            correctCode = removePosition(correctCode, parityBitPosition - i);
        }
        return correctCode;
    }

    private static String removePosition(String message, int position) {
        String beforePosition = message.substring(0, position);
        String afterPosition = message.substring(position + 1);
        return beforePosition + afterPosition;
    }

    static String correctCode(String codedMessage) {
        Binary coded = new Binary(codedMessage);
        String value = coded.getValue();
        int nParityBits = (int) Math.floor(Math.log(coded.convertToBinary(true).length()) / Math.log(2)) + 1;
        int positionToFlip = 0;
        boolean isCorrect = true;
        for (int i = 0; i < nParityBits; i++) {
            int offset = (int) Math.pow(2, i);
            String message = getBitsToCheckParity(value, offset);
            if (!isPair(message)) {
                positionToFlip += offset;
                isCorrect = false;
            }
        }
        if (!isCorrect) {
            coded.flipBit(positionToFlip - 1);
            logger.info("Change bit in position " + (positionToFlip - 1) + " from " + value + " to " + coded.getValue());
        }
        return coded.getValue();
    }

    private static String getBitsToCheckParity(String value, int offset) {
        StringBuilder message = new StringBuilder();
        boolean flag = true;
        for (int j = offset - 1; j < value.length(); j = j + offset) {
            if (flag) {
                if ((j + offset) > value.length()) {
                    message.append(value.substring(j));
                } else {
                    message.append(value, j, j + offset);
                }
            }
            flag = !flag;
        }
        return message.toString();
    }
}
