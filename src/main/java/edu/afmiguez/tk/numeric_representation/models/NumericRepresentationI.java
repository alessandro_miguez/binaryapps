package edu.afmiguez.tk.numeric_representation.models;

public interface NumericRepresentationI {
    String convertToDecimal(boolean is2Complement) throws Exception;
    String convertToHexadecimal(boolean is2Complement);
    String convertToBinary(boolean is2Complement);
}
