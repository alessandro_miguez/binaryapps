package edu.afmiguez.tk.numeric_representation.models.mips;

import edu.afmiguez.tk.numeric_representation.models.Binary;
import edu.afmiguez.tk.numeric_representation.models.Hexadecimal;
import edu.afmiguez.tk.numeric_representation.models.NumericRepresentationI;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class JInstruction extends InstructionAbstract {
    private Binary optcode;
    private Binary address;

    public JInstruction(NumericRepresentationI numericRepresentationI) {
        super(numericRepresentationI);
    }

    @Override
    public Binary getBinary() {
        return new Binary(optcode.getValue()+address.getValue());
    }

    public JInstruction() {
    }

    public JInstruction(String instruction) {
        this();
        parseBinary(instruction);
    }

    @Override
    public void parseBinary(String binary) {
        this.optcode=new Binary(binary.substring(0,6));
        this.address=new Binary(binary.substring(6));
    }

    @Override
    public String getInstruction() {
        InstructionDetails instructionDetails=InstructionAbstract.optFunctInstructionDetailsMap.get(this.optcode.getValue());
        return instructionDetails.getName()+" "+"0x0"+new Hexadecimal(this.address.getValue()+"00").getValue();
    }
}
