package edu.afmiguez.tk.numeric_representation.models.arq;

public interface ARQAlgorithms {
    float getUtilization();
    float getFrameTime();
    float getPropagationTime();
    String getName();
}
