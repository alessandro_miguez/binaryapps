package edu.afmiguez.tk.numeric_representation.models;

import lombok.Data;

@Data
public class Binary implements NumericRepresentationI{
    private String value;

    public Binary(String value, int nBits){
        this(value);
        if(value.length()<nBits){
            int excess=nBits-value.length();
            StringBuilder padding= new StringBuilder();
            for(int i=0;i<excess;i++){
                padding.append("0");
            }
            try {
                this.setValue(padding.toString()+this.getValue());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            try {
                this.setValue(this.trimToNBits(nBits));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Binary(String value)  {
        try {
            this.setValue(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Binary(){   }

    protected void setValue(String value) throws Exception {
        for(Character c:value.toCharArray()){
            if(c !='0' && c !='1' && c !=' '){
                throw new Exception();
            }
        }
        this.value = value;
    }

    private String trimToNBits(int nbits){
        return this.getValue().substring(this.getValue().length()-nbits);
    }

    @Override
    public String convertToDecimal(boolean is2Complement){
        String reverse=new StringBuilder(value).reverse().toString();
        int size=reverse.length();
        char[] reverseArray=reverse.toCharArray();
        long total=0;
        for(int i=0;i<size;i++){
            int digit=Integer.parseInt(""+reverseArray[i]);
            if(i==size-1 && is2Complement){
                total-=digit*Math.pow(2,i);
            }else{
                total+=digit*Math.pow(2,i);
            }
        }
        return ""+total;
    }

    @Override
    public String convertToHexadecimal(boolean is2Complement) {
        return new Hexadecimal(this.value).getValue();
    }

    @Override
    public String convertToBinary(boolean is2Complement) {
        return this.getValue();
    }

    protected void sumOne() {
        char[] array=this.getValue().toCharArray();
        int size=this.getValue().length()-1;
        if(this.getValue().charAt(size)=='0'){
            array[size]='1';
        }else{
            for(int i=size;i>=0;i--){
                char bit=this.getValue().charAt(i);
                if(bit=='0'){
                    array[i]='1';
                    break;
                }else{
                    array[i]='0';
                }
            }
        }
        try {
            this.setValue(String.copyValueOf(array));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void minusOne() {
        char[] array=this.getValue().toCharArray();
        int size=this.getValue().length()-1;
        if(this.getValue().charAt(size)=='1'){
            array[size]='0';
        }else{
            for(int i=size;i>=0;i--){
                char bit=this.getValue().charAt(i);
                if(bit=='1'){
                    array[i]='0';
                    break;
                }else{
                    array[i]='1';
                }
            }
        }
        try {
            this.setValue(String.copyValueOf(array));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void add(Binary bin, boolean is2Complement) {
        long toAdd=Long.parseLong(bin.convertToDecimal(is2Complement));
        for(int i=0;i<toAdd;i++){
            this.sumOne();
        }
    }

    protected void subtract(Binary bin, boolean is2Complement) {
        long toSubtract=Long.parseLong(bin.convertToDecimal(is2Complement));
        for(int i=0;i<toSubtract;i++){
            this.minusOne();
        }
    }

    protected void not(){
        char[] array=this.getValue().toCharArray();
        int size=this.getValue().length();
        for(int i=0;i<size;i++){
            if(array[i]=='0'){
                array[i]='1';
            }else{
                array[i]='0';
            }
        }
        try {
            this.setValue(String.copyValueOf(array));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String stuffbit(String bitToStuff, int position){
        String value=this.getValue();
        if(position<=0)return bitToStuff+value;
        if(bitToStuff.equals("0") || bitToStuff.equals("1") || bitToStuff.equals(" ")){
            String beforeStuffing=value.substring(0,position);
            String afterStuffing=value.substring(position);
            return beforeStuffing+bitToStuff+afterStuffing;
        }
        return value;
    }

    protected void stuffBit(String bitToStuff, int position){
        try {
            this.setValue(stuffbit(bitToStuff,position));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String changebit(String bitToPut, int positionToChange){
        String value=this.getValue();
        char[]array=value.toCharArray();
        array[positionToChange]=bitToPut.charAt(0);
        return String.copyValueOf(array);
    }

    protected void changeBit(String bitToPut, int positionToChange){
        try {
            this.setValue(changebit(bitToPut,positionToChange));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void flipBit(int positionToFlip){
        String bit=this.getValue().charAt(positionToFlip)+"";
        String bitToFlip;
        if(bit.equals("1")){
            bitToFlip="0";
        }else{
            bitToFlip="1";
        }
        changeBit(bitToFlip,positionToFlip);
    }

}
