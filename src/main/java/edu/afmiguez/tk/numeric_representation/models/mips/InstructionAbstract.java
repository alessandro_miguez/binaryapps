package edu.afmiguez.tk.numeric_representation.models.mips;

import com.fasterxml.jackson.annotation.JsonIgnore;
import edu.afmiguez.tk.numeric_representation.models.Binary;
import edu.afmiguez.tk.numeric_representation.models.Decimal;
import edu.afmiguez.tk.numeric_representation.models.Hexadecimal;
import edu.afmiguez.tk.numeric_representation.models.NumericRepresentationI;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public abstract class InstructionAbstract implements InstructionI {

    private String instruction;
    @JsonIgnore
    protected static Map<String,InstructionDetails> instructionDetailsMap=new HashMap<>();
    @JsonIgnore
    protected static Map<String,InstructionDetails> optFunctInstructionDetailsMap =new HashMap<>();
    @JsonIgnore
    protected static Map<String,Binary> registers=new HashMap<>();
    @JsonIgnore
    protected static Map<String,String> reverseRegisters=new HashMap<>();
    static{
        registers.put("$0",new Binary("00000"));
        registers.put("$at",new Binary("00001"));
        registers.put("$v0",new Binary("00010"));
        registers.put("$v1",new Binary("00011"));
        registers.put("$a0",new Binary("00100"));
        registers.put("$a1",new Binary("00101"));
        registers.put("$a2",new Binary("00110"));
        registers.put("$a3",new Binary("00111"));
        registers.put("$t0",new Binary("01000"));
        registers.put("$t1",new Binary("01001"));
        registers.put("$t2",new Binary("01010"));
        registers.put("$t3",new Binary("01011"));
        registers.put("$t4",new Binary("01100"));
        registers.put("$t5",new Binary("01101"));
        registers.put("$t6",new Binary("01110"));
        registers.put("$t7",new Binary("01111"));
        registers.put("$s0",new Binary("10000"));
        registers.put("$s1",new Binary("10001"));
        registers.put("$s2",new Binary("10010"));
        registers.put("$s3",new Binary("10011"));
        registers.put("$s4",new Binary("10100"));
        registers.put("$s5",new Binary("10101"));
        registers.put("$s6",new Binary("10110"));
        registers.put("$s7",new Binary("10111"));
        registers.put("$t8",new Binary("11000"));
        registers.put("$t9",new Binary("11001"));
        registers.put("$k0",new Binary("11010"));
        registers.put("$k1",new Binary("11011"));
        registers.put("$gp",new Binary("11100"));
        registers.put("$sp",new Binary("11101"));
        registers.put("$fp",new Binary("11110"));
        registers.put("$ra",new Binary("11111"));

        String instructionsList="add;R;0;20\n" + "addi;I;8;\n" + "addiu;I;9;\n" + "addu;R;0;21\n" + "and;R;0;24\n" + "andi;I;c;\n" + "beq;I;4;\n" + "bne;I;5;\n" + "j;J;2;\n" + "jal;J;3\n" + "jr;R;0;08\n" + "lbu;I;24;\n" + "lhu;I;25;\n" + "ll;I;30;\n" + "lui;I;f;\n" + "lw;I;23;\n" + "nor;R;0;27\n" + "or;R;0;25\n" + "ori;I;d;\n" + "slt;R;0;2a\n" + "slti;I;a;\n" + "sltiu;I;b;\n" + "sltu;R;0;2b\n" + "sll;R;0;00\n" + "srl;R;0;02\n" + "sb;I;28;\n" + "sc;I;38;\n" + "sh;I;29;\n" + "sw;I;2b;\n" + "sub;R;0;22\n" + "subu;R;0;23";
        String[] lines=instructionsList.split("\n");
        for(String line:lines){
            String[] instructionDetails=line.split(";");
            instructionDetailsMap.put(instructionDetails[0],new InstructionDetails(instructionDetails));
        }

        String opt_func="R100000;add;R\n" + "001000;addi;I\n" + "001001;addiu;I\n" + "R100001;addu;R\n" + "R100100;and;R\n" + "001100;andi;I\n" + "000100;beq;I\n" + "000101;bne;I\n" + "000010;j;J\n" + "000011;jal;J\n" + "R001000;jr;R\n" + "100100;lbu;I\n" + "100101;lhu;I\n" + "110000;ll;I\n" + "001111;lui;I\n" + "100011;lw;I\n" + "R100111;nor;R\n" + "R100101;or;R\n" + "001101;ori;I\n" + "R101010;slt;R\n" + "001010;slti;I\n" + "001011;sltiu;I\n" + "R101011;sltu;R\n" + "R000000;sll;R\n" + "R000010;srl;R\n" + "101000;sb;I\n" + "111000;sc;I\n" + "101001;sh;I\n" + "101011;sw;I\n" + "R100010;sub;R\n" + "R100011;subu;R";
        lines=opt_func.split("\n");
        for(String line:lines){
            String[] instructionDetails=line.split(";");
            optFunctInstructionDetailsMap.put(instructionDetails[0],instructionDetailsMap.get(instructionDetails[1]));
        }

        for(String key:registers.keySet()){
            String value=registers.get(key).getValue();
            reverseRegisters.put(value,key);
        }
    }



    InstructionAbstract() {
    }


    InstructionAbstract(NumericRepresentationI numericRepresentationI) {
        String binary=numericRepresentationI.convertToBinary(false);
            if(binary.length()!=32){
            try {
                throw new IllegalArgumentException("Invalid size of hexadecimal instruction");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.parseBinary(binary);
    }

    /*
        TODO convert a hexadecimal code to instruction. Include logic to JInstruction
            extract optcode, check instruction type, get instruction fields, convert to instruction
    */
    public static InstructionAbstract convertHexadecimalToInstruction(Hexadecimal hexadecimal){
        String binary=hexadecimal.convertToBinary(false);
        String optcode=binary.substring(0,6);
        switch (optcode) {
            case "000000":
                return new RInstruction(binary);
            case "000010":
                return new JInstruction(binary);
            default:
                return new IInstruction(binary);
        }
    }

    public static InstructionAbstract getInstruction(String instruction){
        String[] parsedInstruction=parseInstruction(instruction);
        String instructionName=parsedInstruction[0];
        InstructionDetails details=instructionDetailsMap.get(instructionName);
        if(parsedInstruction.length==2){
            //J type instructions
            if(!details.getType().equals("J")){
                try {
                    throw new IllegalArgumentException("Type missmatch");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            /*
            TODO what is the deal with immediates and addresses????
             */
            String optcodeBin=new Binary(new Hexadecimal("0x"+details.getOptcode()).convertToBinary(false),6).getValue();
            String addressBin=new Hexadecimal(parsedInstruction[1]).convertToBinary(false);
            return new JInstruction(new Binary(optcodeBin+addressBin.substring(0,26)));
        }
        String optcodeBin=new Binary(new Hexadecimal("0x"+details.getOptcode()).convertToBinary(false),6).getValue();

        if(details.getType().equals("R") && parsedInstruction.length==4){
            String rt;
            String rd=registers.get(parsedInstruction[1]).getValue();
            String rs;
            String shamt;
            if(isShift(instructionName)){
                rs=registers.get(parsedInstruction[2]).getValue();
                rt=registers.get(parsedInstruction[3]).getValue();
                shamt="00000";
            }
            /*
            TODO include jr and jalr
             */
            else{
                rs="00000";
                rt=registers.get(parsedInstruction[2]).getValue();
                shamt=new Binary(new Decimal(parsedInstruction[3]).convertToBinary(true,5).getValue(),5).getValue();
            }
            String function=new Binary(new Hexadecimal("0x"+details.getFunction()).convertToBinary(false),6).getValue();
            return new RInstruction(new Binary(optcodeBin+rs+rt+rd+shamt+function));
        }else if("I".equals(details.getType()) ){
            String rt;
            String rs;
            String immediate;
            if(parsedInstruction.length==4){
                if("bne".equals(instructionName) || "beq".equals(instruction)){
                    rt = registers.get(parsedInstruction[2]).getValue();
                    rs = registers.get(parsedInstruction[1]).getValue();
                }else {
                    rt = registers.get(parsedInstruction[1]).getValue();
                    rs = registers.get(parsedInstruction[2]).getValue();
                }
                immediate=new Binary(new Decimal(parsedInstruction[3]).convertToBinary(true,16).getValue(),16).getValue();
            }

            /*
            TODO how to map beq, bne, saves and loads
             */
            else{
                if(isStoreLoad(instructionName)){
                    rt=registers.get(parsedInstruction[1]).getValue();
                    String[] reparsed=parsedInstruction[2].split("[()]");
                    rs=registers.get(reparsed[1]).getValue();
                    immediate=new Binary(new Decimal(reparsed[0]).convertToBinary(true,16).getValue(),16).getValue();
                }
                else{
                    return null;
                }
            }
            return new IInstruction(new Binary(optcodeBin+rs+rt+immediate));
        }
        return null;
    }

    protected static boolean isShift(String instructionName){
        return !"sll".equals(instructionName) && !"srl".equals(instructionName);
    }

    private static boolean isStoreLoad(String instructionName){
        return 'l'==instructionName.charAt(0) || "sb".equals(instructionName) || "sh".equals(instructionName) || "sw".equals(instructionName);
    }

    private static String[] parseInstruction(String instruction){
        return instruction.trim().split("[,\\s]");
    }

    protected static Binary getRegisterBinary(String key){
        return registers.get(key);
    }

    protected static Map<String, InstructionDetails> getInstructionDetailsMap() {
        return instructionDetailsMap;
    }


    public Hexadecimal getHexadecimal() {
        return new Hexadecimal(getBinary());
    }

}
