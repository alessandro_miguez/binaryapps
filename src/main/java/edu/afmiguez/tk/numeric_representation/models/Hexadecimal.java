package edu.afmiguez.tk.numeric_representation.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

@Data
public class Hexadecimal implements NumericRepresentationI {
    private String value;
    @Setter(value = AccessLevel.PRIVATE)
    @ToString.Exclude
    @JsonIgnore
    private static Map<String,String> hexToBinValues =new HashMap<String,String>(){{
        put("0","0000");
        put("1","0001");
        put("2","0010");
        put("3","0011");
        put("4","0100");
        put("5","0101");
        put("6","0110");
        put("7","0111");
        put("8","1000");
        put("9","1001");
        put("A","1010");
        put("B","1011");
        put("C","1100");
        put("D","1101");
        put("E","1110");
        put("F","1111");
    }};
    @ToString.Exclude
    @JsonIgnore
    private static Map<String,String> binToHex=new HashMap<String,String>(){{
        put("0000","0");
        put("0001","1");
        put("0010","2");
        put("0011","3");
        put("0100","4");
        put("0101","5");
        put("0110","6");
        put("0111","7");
        put("1000","8");
        put("1001","9");
        put("1010","A");
        put("1011","B");
        put("1100","C");
        put("1101","D");
        put("1110","E");
        put("1111","F");
    }};

    public Hexadecimal(){

    }

    public Hexadecimal(Binary binary)  {
        this(binary.getValue());
    }

    public Hexadecimal(String value) {
        String aux=value;
        if(aux.contains("0x")){
            try {
                this.setValue(aux.replace("0x", ""));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            try {
                if (aux.length() % 4 != 0) {
                    String bitPadding = aux.charAt(0) + "";
                    int numberOfPaddings = 4 - aux.length() % 4;
                    StringBuilder valueBuilder = new StringBuilder(aux);
                    for (int i = 0; i < numberOfPaddings; i++) {
                        valueBuilder.insert(0, bitPadding);
                    }
                    aux = valueBuilder.toString();
                }
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < aux.length(); i = i + 4) {
                    String hexadecimal = aux.substring(i, i + 4);
                    sb.append(binToHex.get(hexadecimal));
                }
                this.setValue(sb.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void setValue(String value) throws IllegalArgumentException {
        String aux=value.toUpperCase();
        for(Character c:aux.toCharArray()){
            if((!(c >='0' && c <='9')&&!(c >='A' && c <='F'))) throw new IllegalArgumentException("invalid hexadecimal character");
        }
        this.value=aux;
    }

    @Override
    public String convertToDecimal(boolean is2Complement) {
        return new Binary(this.convertToBinary()).convertToDecimal(is2Complement);
    }

    @Override
    public String convertToHexadecimal(boolean is2Complement) {
        return this.getValue();
    }

    @Override
    public String convertToBinary(boolean is2Complement) {
        StringBuilder sb=new StringBuilder();
        for(char s:this.value.toCharArray()){
            sb.append(hexToBinValues.get(s+""));
        }
        return sb.toString();
    }

    protected String convertToBinary() {
        StringBuilder sb=new StringBuilder();
        for(Character c:this.value.toCharArray()){
            c=Character.toUpperCase(c);
            sb.append(hexToBinValues.get(""+Character.toUpperCase(c)));
        }
        return sb.toString();
    }

}
