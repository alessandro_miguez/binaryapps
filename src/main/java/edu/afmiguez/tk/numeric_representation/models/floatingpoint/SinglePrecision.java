package edu.afmiguez.tk.numeric_representation.models.floatingpoint;

import edu.afmiguez.tk.numeric_representation.models.Binary;
import edu.afmiguez.tk.numeric_representation.models.Decimal;
import edu.afmiguez.tk.numeric_representation.models.Hexadecimal;
import edu.afmiguez.tk.numeric_representation.models.NumericRepresentationI;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SinglePrecision implements NumericRepresentationI {

    private Hexadecimal value;

    protected SinglePrecision(float value){
        this.value=convertFloatToHexa(value);
    }

    /*
    TODO convert float to hexadecimal
    first separate integer value from decimal
    for integer values, divide the value by 2, get remainder of that division until value gets to 0
    bits should be in the reverse order
    for the decimal value, multiply the value until either result is 0 OR operations equal to 23 (24 if original decimal value is 0)
    exponent will be the number of bits from the integer value until the last bit 1 from right to left PLUS bias (127)
    mantissa will be the integer values concatenated with decimal bits until 23 bits total

     */
    public Hexadecimal convertFloatToHexa(float value) {
        int integerValue=(int)Math.abs(value);
        int bias=127;
        float decimalValue=Math.abs(value)-integerValue;
        StringBuilder integerBits=new StringBuilder();
        char sign=value>=0?'0':'1';
        while(integerValue>0){
            integerBits.append(integerValue%2);
            integerValue/=2;
        }
        if(integerBits.length()==0){
            integerBits.append("0");
        }
        int i=0;
        StringBuilder decimalBits=new StringBuilder();
        while(i<24 && decimalValue!=0.0f){
            decimalValue*=2;
            if(decimalValue>=1.0){
                decimalBits.append('1');
                decimalValue-=1.0;
            }else{
                decimalBits.append('0');
            }
            i++;
        }
        int exp;
        if(integerBits.length()==1){
            exp=bias-1;
        }else{
            exp=integerBits.length()-1+bias;
        }
        String mantissa;
        if(integerBits.length()==1){
            mantissa=decimalBits.toString().substring(1);
        }else{
            mantissa=integerBits.reverse().substring(1)+decimalBits.toString();
        }

        if(mantissa.length()<23){
            StringBuilder stringBuilder=new StringBuilder(mantissa);
            for(i=mantissa.length();i<23;i++){
                stringBuilder.append('0');
            }
            mantissa=stringBuilder.toString();
        }
        String exponentBin=new Decimal(""+exp).convertToBinary(false,8).getValue();
        return new Hexadecimal(sign+exponentBin+mantissa);
    }

    SinglePrecision(String value){
        this.value=new Hexadecimal(value);
    }

    /*
    TODO check more if this works
     */
    @Override
    public String convertToDecimal(boolean is2Complement) throws Exception {
        String binary=this.value.convertToBinary(false);
        int bias=127;
        int sign=binary.charAt(0)=='0'?1:-1;
        String exp=binary.substring(1,9);
        String mantissa=binary.substring(9);
        int expDec=Integer.parseInt(new Binary(exp).convertToDecimal(false))-bias;
        float mantissaDec=1.0f;

        for(int i=0;i<mantissa.length();i++){
            char c=mantissa.charAt(i);
            if(c=='1'){
                mantissaDec+=Math.pow(2,-(i+1));
            }
        }
        return ""+sign*mantissaDec*Math.pow(2,expDec);
    }

    @Override
    public String convertToHexadecimal(boolean is2Complement) {
        return value.getValue();
    }

    @Override
    public String convertToBinary(boolean is2Complement) {
        return value.convertToBinary(false);
    }
}
