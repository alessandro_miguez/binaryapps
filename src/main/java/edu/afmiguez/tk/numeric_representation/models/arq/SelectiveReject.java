package edu.afmiguez.tk.numeric_representation.models.arq;

public class SelectiveReject extends SlidingWindow{

    public SelectiveReject(SlidingWindow stopAndWait) {
        super(stopAndWait);
        this.setWindows(((int)Math.pow(2,this.getBits()-1)));
        this.setU(this.getU());
    }

    @Override
    public String getName() {
        return "Selective Reject";
    }
}
