package edu.afmiguez.tk.numeric_representation.models.mips;

import edu.afmiguez.tk.numeric_representation.models.Binary;
import edu.afmiguez.tk.numeric_representation.models.NumericRepresentationI;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class RInstruction extends InstructionAbstract{
    private Binary optcode;
    private Binary rs;
    private Binary rt;
    private Binary rd;
    private Binary shamt;
    private Binary function;

    RInstruction() {}

    RInstruction(NumericRepresentationI numericRepresentationI) {
        super(numericRepresentationI);
    }

    RInstruction(String instruction) {
        this();
        parseBinary(instruction);
    }

    public void parseBinary(String binary){
        this.optcode=new Binary(binary.substring(0,6));
        this.rs=new Binary(binary.substring(6,11));
        this.rt=new Binary(binary.substring(11,16));
        this.rd=new Binary(binary.substring(16,21));
        this.shamt =new Binary(binary.substring(21,26));
        this.function=new Binary(binary.substring(26));
    }

    @Override
    public Binary getBinary() {
        return new Binary(optcode.getValue()+rs.getValue()+rt.getValue()+rd.getValue()+ shamt.getValue()+function.getValue());
    }


    @Override
    public String getInstruction() {
        InstructionDetails instructionDetails=InstructionAbstract.optFunctInstructionDetailsMap.get("R"+this.getFunction().getValue());
        if(isShift(instructionDetails.getName())){
            return instructionDetails.getName()+" "+reverseRegisters.get(getRd().getValue())+","+reverseRegisters.get(getRs().getValue())+","+reverseRegisters.get(getRt().getValue());
        }else{
            return instructionDetails.getName()+" "+reverseRegisters.get(getRd().getValue())+","+reverseRegisters.get(getRt().getValue())+","+shamt.convertToDecimal(true);
        }
    }
}
