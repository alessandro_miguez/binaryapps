package edu.afmiguez.tk.numeric_representation.services;

import edu.afmiguez.tk.numeric_representation.models.Hexadecimal;
import edu.afmiguez.tk.numeric_representation.models.mips.InstructionAbstract;
import org.springframework.stereotype.Service;

@Service
public class ACPTService {

    public InstructionAbstract convertInstruction(String instruction) {
        return InstructionAbstract.convertHexadecimalToInstruction(new Hexadecimal("0x"+instruction));
    }
}
