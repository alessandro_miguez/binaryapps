package edu.afmiguez.tk.numeric_representation.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CommonController {
    @GetMapping("/rco1")
    public String getRCO(Model model) {

        model.addAttribute("name", "TESTE");
        model.addAttribute("title", "RCO1");
        return "rco1";
    }
    @GetMapping("/acpt")
    public String getACPT(Model model) {

        model.addAttribute("name", "TESTE");
        model.addAttribute("title", "ACPT");
        return "acpt";
    }
}

