package edu.afmiguez.tk.numeric_representation.controllers;

import edu.afmiguez.tk.numeric_representation.models.mips.InstructionAbstract;
import edu.afmiguez.tk.numeric_representation.services.ACPTService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RequestMapping("/acpt")
@RestController
public class ACPTController {

    private ACPTService acptService;

    public ACPTController(ACPTService acptService) {
        this.acptService = acptService;
    }

    @PutMapping(value ="hexadecimal",consumes = "text/plain")
    public ResponseEntity<InstructionAbstract> convertInstruction(@RequestBody String instruction){
        return ResponseEntity.ok(acptService.convertInstruction(instruction));
    }

    @PutMapping(value ="instruction")
    public ResponseEntity<InstructionAbstract> convertHexadecimal(@RequestBody String instruction){
        return ResponseEntity.ok(Objects.requireNonNull(InstructionAbstract.getInstruction(instruction)));
    }
}
