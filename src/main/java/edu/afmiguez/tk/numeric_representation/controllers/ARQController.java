package edu.afmiguez.tk.numeric_representation.controllers;

import edu.afmiguez.tk.numeric_representation.models.arq.SelectiveReject;
import edu.afmiguez.tk.numeric_representation.models.arq.SlidingWindow;
import edu.afmiguez.tk.numeric_representation.models.arq.StopAndWait;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/arq")
@RestController
public class ARQController {

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<StopAndWait>> calculateStopAndWait(@RequestBody SlidingWindow stopAndWait){
        List<StopAndWait> results=new ArrayList<>();

        StopAndWait result=new StopAndWait(stopAndWait);
        if(result.getTp()==null || result.getTf()==null || result.getU()==null || result.getA()==null){
            return ResponseEntity.badRequest().build();
        }
        results.add(result);

        SlidingWindow slidingWindow=new SlidingWindow(stopAndWait);

        results.add(slidingWindow);

        SelectiveReject selectiveReject=new SelectiveReject(stopAndWait);

        results.add(selectiveReject);

        return ResponseEntity.ok(results);
    }
}
